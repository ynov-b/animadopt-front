import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        "gradient-start": "#F9B16E",
        "gradient-end": "#F68080",
        "primary-100": "#FFFAF6",
        "primary-200": "#ECD5C5",
        "primary-500": "#EB6A08",
        "info-400": "#17A4E6",
        "info-100": "#E5F7FF",
      },
    },
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    fontFamily: {
      montserrat: "var(--font-monsterat)",
      cairo: "var(--font-cairo)",
      oxygen: "var(--font-oxygen)",
    },
  },
  plugins: [],
};
export default config;
