const traductSpecies = (string: string) => {
  switch (string.toLowerCase()) {
    case "dog":
      return "Chien";
    case "cat":
      return "Chat";
    case "rabbit":
      return "Lapin";
    case "bird":
      return "Oiseau";
    case "rodent":
      return "Rongeur";
    case "fish":
      return "Poisson";
    case "wabbit":
      return "Lapin";
    case "hamster":
      return "Hamster";
    case "guinea_pig":
      return "Cochon d'Inde";
    case "snake":
      return "Serpent";
    case "lizard":
      return "Lézard";
    case "turtle":
      return "Tortue";
    case "ferret":
      return "Furet";
    case "horse":
      return "Cheval";
    default:
      return "Autre";
  }
};

export default traductSpecies;
