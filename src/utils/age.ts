export const calculateAge = (timestamp: string) => {
  const birthDate = new Date(timestamp);
  const currentDate = new Date();
  const difference = currentDate.getTime() - birthDate.getTime();
  const ageInYears = difference / (1000 * 60 * 60 * 24 * 365.25);
  return Math.floor(ageInYears);
};
