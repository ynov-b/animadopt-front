import React from "react";
import {
  Card,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";

interface Props {
  animals: Animal[] | undefined;
  isFetching: boolean;
}

const AssociationAnimalsList: React.FC<Props> = ({ animals, isFetching }) => {
  if (isFetching) {
    return (
      <div>
        <p>Loading...</p>
      </div>
    );
  }

  if (animals === undefined) {
    return (
      <div>
        <p>noAnimals</p>
      </div>
    );
  }

  return (
    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
      {animals.map((animal) => (
        <Card key={animal.animalId} className="cursor-pointer">
          <CardHeader className="p-4">
            <CardTitle className="text-lg font-semibold text-primary-350">
              {animal.name}
            </CardTitle>
            <CardDescription className="text-gray-600">
              <b>Species:</b> {animal.species}
              <b>Breed:</b> {animal.breed}
              <b>Age:</b> {animal.birthday}
            </CardDescription>
          </CardHeader>
        </Card>
      ))}
    </div>
  );
};

export default AssociationAnimalsList;
