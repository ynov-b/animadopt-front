import React, { useEffect, useState } from "react";

const Clock = () => {
  const [time, setTime] = useState(new Date());

  useEffect(() => {
    const timer = setInterval(() => {
      setTime(new Date());
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <div className="flex flex-row h-full justify-center align-middle flex-grow">
      <p className="font-cairo font-bold text-white text-6xl pt-32">
        {time.toLocaleTimeString("fr-FR")}
      </p>
    </div>
  );
};

export default Clock;
