import React from "react";
import { Line } from "react-chartjs-2";
import {
  Chart,
  LinearScale,
  CategoryScale,
  PointElement,
  LineElement,
} from "chart.js";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";

Chart.register(LinearScale, CategoryScale, PointElement, LineElement);

const data = {
  labels: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin"],
  datasets: [
    {
      label: "# de Requêtes",
      data: [12, 19, 3, 5, 2, 3],
      fill: false,
      backgroundColor: "rgb(255, 99, 132)",
      borderColor: "rgba(255, 99, 132, 0.2)",
      responsive: true,
      maintainAspectRatio: false,
    },
  ],
};

const options = {
  scales: {
    y: {
      beginAtZero: true,
    },
  },
};

const RequestsCharts = () => {
  return (
    <Card className="bg-white mt-8">
      <CardHeader>
        <CardTitle className="font-normal">Demandes</CardTitle>
        <CardDescription className="text-slate-500">
          Evolution du nombre de demandes par mois
        </CardDescription>
      </CardHeader>
      <CardContent className="mb-4">
        <Line data={data} options={options} />
      </CardContent>
      <CardFooter>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-6 h-6 text-red-600"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M2.25 6L9 12.75l4.286-4.286a11.948 11.948 0 014.306 6.43l.776 2.898m0 0l3.182-5.511m-3.182 5.51l-5.511-3.181"
          />
        </svg>
        <p className="ml-2">En baisse</p>
      </CardFooter>
    </Card>
  );
};

export default RequestsCharts;
