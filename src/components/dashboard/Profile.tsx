'use client'
import React, {useState} from "react";
import { useQuery } from "@tanstack/react-query";
import { apiGetAssociationId } from "@/shared/api/associations";
import { useParams, useRouter } from "next/navigation";
import { Params } from "next/dist/shared/lib/router/utils/route-matcher";
import { Card } from "@/components/ui/card";
import ProfileForm from "@/components/dashboard/ProfileForm";
import {AssociationType} from "@/shared/type/associations";

interface AssociationsIdPageParams extends Params {
  id: string;
}

const fakeAssociationData = {
  assoId: 1,
  name: "Association Animaux Bien-Être",
  localisation: "Ville fictive",
  email: "contact@association-animaux.org",
  phone: "123-456-7890",
  codeAsso: 1234,
  nbPets: 50,
  speciesList: ["Chien", "Chat", "Oiseau", "Lapin"],
  animals: [
    { id: 1, name: "Fido", species: "Chien" },
    { id: 2, name: "Whiskers", species: "Chat" },
    // Ajoutez d'autres animaux fictifs au besoin
  ],
};

const AssociationProfilePage = () => {
  // const params = useParams<AssociationsIdPageParams>();
  // const router = useRouter();
  // const id = +params.id;

  // const { data: association, isLoading, isError } = useQuery({
  //   queryKey: ["associationDetail"],
  //   queryFn: () => apiGetAssociationId(id),
  // });

  // if (isLoading) {
  //   return <div>Chargement en cours...</div>;
  // }
  //
  // if (isError) {
  //   return (
  //     <div>
  //       Une erreur s'est produite lors de la récupération des détails de
  //       l'association
  //     </div>
  //   );
  // }
  const [isProfileFormOpen, setProfileFormOpen] = useState(false);
  const [associationData, setAssociationData] = useState(fakeAssociationData);

  const handleEditProfile = () => {
    setProfileFormOpen(true);
  };

  const handleCloseProfileForm = () => {
    setProfileFormOpen(false);
  };

  const handleUpdateAssociation = (updatedData: any) => {
    setAssociationData(updatedData);
  };

  return (
    <div className="flex min-h-screen flex-col bg-primary-350 text-white mx-auto container">
      <div className="bg-white p-4 rounded-lg w-full max-w-screen-lg justify-center space-y-14">
        <h1 className="text-4xl font-semibold text-primary text-gradient-end">
          {fakeAssociationData.name}
        </h1>
        <p className="text-gray-600">
          <b>Localisation:</b> {fakeAssociationData.localisation}
        </p>
        <p className="text-gray-600">
          <b>Email:</b> {fakeAssociationData.email}
        </p>
        <p className="text-gray-600">
          <b>Téléphone:</b> {fakeAssociationData.phone}
        </p>
        <p className="text-gray-600">
          <b>Nombre d'animaux:</b> {fakeAssociationData.nbPets}
        </p>
        <div>
          <b>Liste des espèces:</b>
          <ul className={"flex gap-2 mt-2"}>
            {fakeAssociationData.speciesList.map((species, index) => (
              <Card
                className={"border-gradient-end text-gradient-end pr-2 pl-2 bg-primary-350"}
                key={index}
              >
                <li>{species}</li>
              </Card>
            ))}
          </ul>
        </div>
      </div>
      <div className={"bg-gradient-end hover:bg-gray-400 px-4 py-2 leading-none border rounded text-primary-350 font-bold drop-shadow-md lg:m-auto"}>
        <button onClick={handleEditProfile}>
          Modifier Profil
        </button>
      </div>
      {isProfileFormOpen && (
        <ProfileForm
          associationData={associationData}
          onUpdateAssociation={handleUpdateAssociation}
          onClose={handleCloseProfileForm}
        />
      )}
    </div>
  );
};

export default AssociationProfilePage;
