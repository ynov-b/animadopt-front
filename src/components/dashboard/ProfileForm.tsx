'use client'
import React, { useState, useEffect } from "react";
import {Species} from "@/shared/enum/SpeciesEnum";

// @ts-ignore
const ProfileForm = ({ associationData, onClose, onUpdateAssociation }) => {
  const [formData, setFormData] = useState({ ...associationData });

  const handleChange = (e: { target: { name: any; value: any; }; }) => {
    const { name, value } = e.target;
    setFormData((prevData: any) => ({ ...prevData, [name]: value }));
  };

  const handleSpeciesChange = (species: Species) => {
    const updatedSpeciesList = formData.speciesList.includes(species)
      ? formData.speciesList.filter((s: Species) => s !== species)
      : [...formData.speciesList, species];

    setFormData((prevData: any) => ({ ...prevData, speciesList: updatedSpeciesList }));
  };

  const handleSubmit = () => {
    onUpdateAssociation(formData);
    onClose();
  };

  useEffect(() => {
    setFormData({ ...associationData });
  }, [associationData]);

  return (
    <div className="fixed inset-0 bg-black bg-opacity-50 flex items-center justify-center">
      <div className="bg-gradient-end p-4 rounded-lg bg-opacity-90">
        <h2 className="text-2xl font-bold mb-4">Modifier le profil</h2>

        <div className="mb-4">
          <label className="block text-sm font-medium text-white mb-1">Nom de l'association</label>
          <input
            type="text"
            name="name"
            value={formData.name}
            onChange={handleChange}
            className="border p-2 w-full text-black"
            placeholder="Nom de l'association"
          />
        </div>

        <div className="mb-4">
          <label className="block text-sm font-medium text-white mb-1">Localisation</label>
          <input
            type="text"
            name="localisation"
            value={formData.localisation}
            onChange={handleChange}
            className="border p-2 w-full text-black"
            placeholder="Localisation"
          />
        </div>

        <div className="mb-4">
          <label className="block text-sm font-medium text-white mb-1">Email</label>
          <input
            type="text"
            name="email"
            value={formData.email}
            onChange={handleChange}
            className="border p-2 w-full text-black"
            placeholder="Email"
          />
        </div>

        <div className="mb-4">
          <label className="block text-sm font-medium text-white mb-1">Téléphone</label>
          <input
            type="text"
            name="phone"
            value={formData.phone}
            onChange={handleChange}
            className="border p-2 w-full text-black"
            placeholder="Téléphone"
          />
        </div>

        <div className="mb-4">
          <label className="block text-sm font-medium text-white mb-1">Nombre d'animaux</label>
          <input
            type="text"
            name="NbPets"
            value={formData.nbPets}
            onChange={handleChange}
            className="border p-2 w-full text-black"
            placeholder="Nombre d'animaux"
          />
        </div>

        <div className="mb-4">
          <label className="block text-sm font-medium text-white mb-1">Espèces</label>
          <div className="flex flex-wrap gap-4">
            {Object.values(Species).map((species) => (
              <div key={species} className="flex items-center">
                <input
                  type="checkbox"
                  id={species}
                  name={species}
                  checked={formData.speciesList.includes(species)}
                  onChange={() => handleSpeciesChange(species)}
                  className="mr-2"
                />
                <label htmlFor={species} className="text-sm">
                  {species}
                </label>
              </div>
            ))}
          </div>
        </div>

        {/* Ajoutez d'autres champs au besoin */}

        <button onClick={handleSubmit} className="bg-white text-gradient-end px-4 py-2 rounded">
          Enregistrer
        </button>
      </div>
    </div>
  );
};

export default ProfileForm;

