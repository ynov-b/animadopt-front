import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import React, { FC, useEffect, useState } from "react";
import { useQuery } from "@tanstack/react-query";
import { apiGetAllAnimalSpecies } from "@/shared/api/animalSpecies";
import { Button } from "@/components/ui/button";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/components/ui/tooltip";
import { apiGetAssociations } from "@/shared/api/associations";

type AnimalFilterProps = {
  onFilterChange: (filter: AnimalFilterType) => void;
};

export const AnimalFilter: FC<AnimalFilterProps> = ({ onFilterChange }) => {
  const [selectedSpecies, setSelectedSpecies] = useState<string>("");
  const [selectedBreed, setSelectedBreed] = useState<string>("");
  const [selectedSex, setSelectedSex] = useState<string>("");
  const [selectedAssociation, setSelectedAssociation] = useState<string>("");
  const [breedsBySpecies, setBreedsBySpecies] = useState<string[]>([]);

  const {
    data: animalSpecies,
    isLoading,
    isError,
  } = useQuery({
    queryKey: ["animalSpecies"],
    queryFn: apiGetAllAnimalSpecies,
  });

  const { data: associationsData } = useQuery({
    queryKey: ["associations"],
    queryFn: apiGetAssociations,
  });

  useEffect(() => {
    handleFilterChange();
  }, [
    selectedSpecies,
    selectedBreed,
    selectedSex,
    selectedAssociation,
    breedsBySpecies,
  ]);

  const handleSpeciesChange = (selectedValue: string) => {
    setSelectedSpecies(selectedValue);
    const breeds = animalSpecies!
      .filter((animal) => animal.speciesName === selectedValue)
      .map((animal) => animal.breedName);

    setBreedsBySpecies(breeds);
  };

  const handleBreedChange = (selectedValue: string) => {
    setSelectedBreed(selectedValue);
  };

  const handleSexChange = (selectedValue: string) => {
    setSelectedSex(selectedValue);
  };

  const handleAssociationChange = (selectedValue: string) => {
    setSelectedAssociation(selectedValue);
  };

  const handleFilterChange = () => {
    const filters: AnimalFilterType = {
      species: selectedSpecies,
      breed: selectedBreed,
      sex: selectedSex,
      association: selectedAssociation,
    };

    onFilterChange(filters);
  };

  const resetFilters = () => {
    setSelectedSpecies("");
    setSelectedBreed("");
    setSelectedSex("");
    setSelectedAssociation("");
    setBreedsBySpecies([]);
  };

  if (isLoading) {
    return (
      <div className="flex min-h-screen flex-col items-center bg-background mx-auto container">
        Chargement en cours...
      </div>
    );
  }

  if (isError) {
    return (
      <div>Une erreur s'est produite lors de la récupération des animaux</div>
    );
  }

  const species = [
    ...new Set(animalSpecies?.map((animal) => animal.speciesName)),
  ];

  return (
    <div className=" p-6 w-full mb-10 bg-orange-50 border-2 rounded-lg border-orange-100">
      <div className="grid lg:grid-cols-12 gap-x-3">
        <Select onValueChange={handleSpeciesChange}>
          <SelectTrigger className="aria-expanded:border-orange-500 text-orange-700 bg-white sm:mb-2 lg:mb-0 col-span-3">
            <SelectValue placeholder="Selectionner une espèce" />
          </SelectTrigger>
          <SelectContent className="bg-white mb-2 text-black border-none">
            <SelectGroup>
              <SelectLabel>Sélectionner une espèce</SelectLabel>

              {species.map((species) => (
                <SelectItem value={species} key={species}>
                  {species}
                </SelectItem>
              ))}
            </SelectGroup>
          </SelectContent>
        </Select>

        <Select onValueChange={handleBreedChange}>
          <SelectTrigger className=" bg-white aria-expanded:border-orange-500 text-orange-700  sm:mb-2 lg:mb-0 col-span-3">
            <SelectValue placeholder="Sélectionner une race" />
          </SelectTrigger>
          <SelectContent className="bg-white border-b-amber-500 text-black">
            <SelectGroup>
              <SelectLabel>Sélectionner une race</SelectLabel>
              {breedsBySpecies.map((breed) => (
                <SelectItem value={breed} key={breed}>
                  {breed}
                </SelectItem>
              ))}
            </SelectGroup>
          </SelectContent>
        </Select>

        <Select onValueChange={handleSexChange}>
          <SelectTrigger className=" bg-white aria-expanded:border-orange-500 text-orange-700  sm:mb-2 lg:mb-0 col-span-2">
            <SelectValue placeholder="Sélectionner le sexe" />
          </SelectTrigger>
          <SelectContent className="bg-white text-black">
            <SelectGroup>
              <SelectLabel>Sélectionner le sexe</SelectLabel>
              <SelectItem value="MALE">Mâle</SelectItem>
              <SelectItem value="FEMALE">Femelle</SelectItem>
            </SelectGroup>
          </SelectContent>
        </Select>

        <Select onValueChange={handleAssociationChange}>
          <SelectTrigger className=" bg-white aria-expanded:border-orange-500 text-orange-700  sm:mb-2 lg:mb-0 col-span-3">
            <SelectValue placeholder="Nom de l'association" />
          </SelectTrigger>
          <SelectContent className="bg-white text-black">
            <SelectGroup>
              <SelectLabel>Sélectionner une association</SelectLabel>
              {associationsData?.map((association) => (
                <SelectItem value={association.name} key={association.assoId}>
                  {association.name}
                </SelectItem>
              ))}
            </SelectGroup>
          </SelectContent>
        </Select>

        <TooltipProvider>
          <Tooltip>
            <TooltipTrigger asChild className="col-span-1">
              <Button
                onClick={resetFilters}
                className="bg-orange-200 text-orange-600 font-bold hover:scale-105"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-5 h-5"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
                  />
                </svg>
              </Button>
            </TooltipTrigger>
            <TooltipContent>
              <p>Réinitialiser les filtres</p>
            </TooltipContent>
          </Tooltip>
        </TooltipProvider>
      </div>
    </div>
  );
};
