import ReactPaginate from "react-paginate";
import React from "react";

interface PaginationProps {
  totalItems: number;
  itemsPerPage: number;
  currentPage: number;
  onPageChange: (selectedItem: any) => void;
}

export const Pagination: React.FC<PaginationProps> = ({
  totalItems,
  itemsPerPage,
  currentPage,
  onPageChange,
}) => {
  return (
    <div className="flex justify-center">
      {totalItems > itemsPerPage && (
        <ReactPaginate
          pageCount={Math.ceil(totalItems / itemsPerPage)}
          pageRangeDisplayed={10}
          marginPagesDisplayed={2}
          onPageChange={onPageChange}
          containerClassName={"flex"}
          activeClassName={"text-orange-600 font-bold "}
          pageClassName={"mx-2 text-sm text-orange-600"}
          previousLabel={"Précédent"}
          previousClassName={"mx-4 text-sm text-gray-400"}
          nextClassName={"mx-4 text-sm text-gray-400"}
          nextLabel={"Suivant"}
        />
      )}
    </div>
  );
};
