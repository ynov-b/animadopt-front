import React from "react";

const DashboardNav = ({
  className,
  setActiveTab,
  activeTab,
}: {
  className: string;
  setActiveTab: React.Dispatch<React.SetStateAction<string>>;
  activeTab: string;
}) => {
  return (
    <div className={className}>
      <h1 className="text-white font-montserrat font-bold text-3xl h-fit mb-20">
        Dashboard
      </h1>
      <nav className="text-xl flex-grow">
        <ul className="flex flex-col gap-8">
          <li className="flex gap-4 font-cairo text-white">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M2.25 12l8.954-8.955c.44-.439 1.152-.439 1.591 0L21.75 12M4.5 9.75v10.125c0 .621.504 1.125 1.125 1.125H9.75v-4.875c0-.621.504-1.125 1.125-1.125h2.25c.621 0 1.125.504 1.125 1.125V21h4.125c.621 0 1.125-.504 1.125-1.125V9.75M8.25 21h8.25"
              />
            </svg>
            <a
              // className="cursor-pointer"
              className={
                activeTab === "Vue globale"
                  ? " font-bold"
                  : " " + "cursor-pointer"
              }
              onClick={(e) => {
                e.preventDefault();
                setActiveTab("Vue globale");
              }}
            >
              Vue globale
            </a>
          </li>
          <li className="flex gap-4 font-cairo text-white">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M9 3.75H6.912a2.25 2.25 0 00-2.15 1.588L2.35 13.177a2.25 2.25 0 00-.1.661V18a2.25 2.25 0 002.25 2.25h15A2.25 2.25 0 0021.75 18v-4.162c0-.224-.034-.447-.1-.661L19.24 5.338a2.25 2.25 0 00-2.15-1.588H15M2.25 13.5h3.86a2.25 2.25 0 012.012 1.244l.256.512a2.25 2.25 0 002.013 1.244h3.218a2.25 2.25 0 002.013-1.244l.256-.512a2.25 2.25 0 012.013-1.244h3.859M12 3v8.25m0 0l-3-3m3 3l3-3"
              />
            </svg>
            <a
              className={
                activeTab === "Demandes" ? " font-bold" : " " + "cursor-pointer"
              }
              onClick={(e) => {
                e.preventDefault();
                setActiveTab("Demandes");
              }}
            >
              Demandes
            </a>
          </li>
          <li className="flex gap-4 font-cairo text-white">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M20.25 6.375c0 2.278-3.694 4.125-8.25 4.125S3.75 8.653 3.75 6.375m16.5 0c0-2.278-3.694-4.125-8.25-4.125S3.75 4.097 3.75 6.375m16.5 0v11.25c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125V6.375m16.5 0v3.75m-16.5-3.75v3.75m16.5 0v3.75C20.25 16.153 16.556 18 12 18s-8.25-1.847-8.25-4.125v-3.75m16.5 0c0 2.278-3.694 4.125-8.25 4.125s-8.25-1.847-8.25-4.125"
              />
            </svg>
            <a
              className={
                activeTab === "Animaux" ? " font-bold" : " " + "cursor-pointer"
              }
              onClick={(e) => {
                e.preventDefault();
                setActiveTab("Animaux");
              }}
            >
              Animaux
            </a>
          </li>
          <li className="flex gap-4 font-cairo text-white">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
              />
            </svg>
            <a
              className={
                activeTab === "Profil" ? " font-bold" : " " + "cursor-pointer"
              }
              onClick={(e) => {
                e.preventDefault();
                setActiveTab("Profil");
              }}
            >
              Profil
            </a>
          </li>
        </ul>
      </nav>
      <button className="text-white-500 flex gap-2 place-end text-white">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-6 h-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75"
          />
        </svg>
        Déconnexion
      </button>
    </div>
  );
};

export default DashboardNav;
