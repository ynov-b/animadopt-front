import Link from "next/link";
import React from "react";

const Footer = () => {
  return (
    <footer className="text-orange-600 w-full p-5 grid lg:grid-cols-3 flex-1 bg-orange-100 md:text-sm mt-32">
      <div className="p-3">
        <Link className={`text-orange-600  font-bold`} href="/animal/list">
          Les animaux
        </Link>
        <p>
          <a href="#">Les associations</a>
        </p>
      </div>

      <div className="p-3">
        <p>
          <a href="#">Politique de confidentialité</a>
        </p>
        <p>
          <a href="#">Conditions d'utilisation</a>
        </p>
      </div>

      <div className="p-3">
        <p>Contact : contact@animadopt.com</p>
        <p>&copy; 2023 Animadop't - Tous droits réservés</p>
      </div>
    </footer>
  );
};

export default Footer;
