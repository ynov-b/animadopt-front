import React, { FC, useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import { FormProvider, SubmitHandler, useForm } from "react-hook-form";
import {
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Textarea } from "@/components/ui/textarea";
import * as z from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

import axios from "axios";
import { API_ENDPOINTS } from "@/shared/endpoint";
import { useMutation } from "@tanstack/react-query";
import { useToast } from "@/components/ui/use-toast";

interface ModalProps {
  animal: Animal;
}

const defaultFormValues = {
  userMessage: "",
};

export const Modal: FC<ModalProps> = ({ animal }) => {
  const [isModalOpen, setIsModalOpen] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const { toast } = useToast();

  const formSchema = z.object({
    userMessage: z.string().min(2).max(1000),
  });

  // Form validation
  const form = useForm<AdoptionRequest>({
    resolver: zodResolver(formSchema),
    defaultValues: defaultFormValues,
  });

  const mutation = useMutation({
    mutationFn: (formData: AdoptionRequest) => {
      return axios.post(
        API_ENDPOINTS.ANIMALS.ADOPTION_REQUEST(animal.animalId),
        formData,
      );
    },
  });

  const handleSubmit: SubmitHandler<AdoptionRequest> = async (formData) => {
    const userId = 1;

    try {
      await mutation.mutateAsync({
        ...formData,
        userId: userId,
      });

      setIsModalOpen(false);
      toast({
        title: "Demande d'adoption envoyée !",
        description: "Nous vous recontacterons dans les plus brefs délais.",
      });
    } catch (e) {
      setError(e.response?.data?.detail || "Une erreur s'est produite.");
    }
  };
  const handleOpenModal = () => {
    form.reset();
    setIsModalOpen(true);
  };

  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button
          className="bg-orange-600 text-orange-100 lg:w-1/2 m-auto hover:scale-105"
          onClick={() => handleOpenModal()}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-5 h-5 mr-3"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
            />
          </svg>
          Demande d'adoption
        </Button>
      </DialogTrigger>
      {isModalOpen && (
        <DialogContent className="sm:max-w-[600px] bg-white text-orange-700">
          <DialogHeader>
            <DialogTitle className="mb-10">Demande d'adoption</DialogTitle>
            <DialogDescription>
              <img
                src={`/images/moufette.jpg`}
                alt="animal-image-modal"
                className="mb-8 rounded-lg w-1/2 m-auto"
              />
              Expliquez-nous brièvement votre motivation à adopter{" "}
              <span className="font-bold">{animal.name} </span> et partagez un
              peu sur votre style de vie. Nous voulons nous assurer que nos
              petits protégés trouvent leur foyer idéal.
            </DialogDescription>
          </DialogHeader>
          <div className="grid gap-y-3 py-4">
            <FormProvider {...form}>
              <form
                onSubmit={form.handleSubmit(handleSubmit)}
                className="space-y-8 flex flex-col"
              >
                <FormField
                  control={form.control}
                  name="userMessage"
                  render={({ field }) => (
                    <FormItem>
                      <FormLabel>Message</FormLabel>
                      <FormControl>
                        <Textarea placeholder="Votre message" {...field} />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                {error && (
                  <p className="text-red-700 text-xs bg-red-200 rounded-lg mt-3 p-3 flex">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="w-5 h-5 mr-3"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M12 9v3.75m-9.303 3.376c-.866 1.5.217 3.374 1.948 3.374h14.71c1.73 0 2.813-1.874 1.948-3.374L13.949 3.378c-.866-1.5-3.032-1.5-3.898 0L2.697 16.126zM12 15.75h.007v.008H12v-.008z"
                      />
                    </svg>
                    Erreur : {error}
                  </p>
                )}
                <Button
                  className="bg-orange-600 text-orange-100 w-1/2 m-auto   hover:scale-105"
                  type="submit"
                  onSubmit={handleSubmit}
                >
                  Envoyer
                </Button>
              </form>
            </FormProvider>
          </div>
        </DialogContent>
      )}
    </Dialog>
  );
};
