import React from "react";
import { Input, InputProps } from "../ui/input";
import { FieldError } from "react-hook-form";

const ErrorInput = React.forwardRef<
  HTMLInputElement,
  InputProps & { error?: FieldError }
>(({ error, ...props }, ref) => {
  return (
    <div className="flex flex-col gap">
      {error && (
        <span className="flex flex-row text-sm text-red-500">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
          {error.message}
        </span>
      )}
      <Input
        ref={ref}
        {...props}
        className={error ? "focus:border-red-500 " : "" + props.className}
      />
    </div>
  );
});

export default ErrorInput;
