import React, { useEffect } from "react";
import AnimalSpecies from "@/app/services/enums/species";
import { WithContext as ReactTags } from "react-tag-input";
import * as yup from "yup";
import { useForm, useWatch } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Form } from "./form";
import ErrorInput from "../errorInput/errorInput";

// Yup validation schema
const schema = yup.object().shape({
  name: yup.string().min(2).max(20).required("Name is required"),
  siret: yup.string().min(9).max(9).required("SIREN is required"),
  animals: yup.string().min(1).max(20).required("Animals is required"),
  species: yup.array().of(yup.string().required()).min(1).required(),
});

const animalSpeciesTable = Object.values(AnimalSpecies);
const suggestions = animalSpeciesTable.map((species) => {
  return {
    id: species,
    text: species,
  };
});

const KeyCodes = {
  comma: 188,
  enter: 13,
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

const AssoFormStepOne = (props: {
  setForm: any;
  form: Form;
  currentFormIsValid: any;
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
    reset,
    control,
    watch,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
    defaultValues: {
      name: props.form.name,
      siret: props.form.siret,
      animals: props.form.animals,
      species: [...props.form.species],
    },
  });

  // Send to the parent component if the form is valid
  let value = useWatch({
    control,
    name: ["name", "siret", "animals", "species"],
  });

  useEffect(() => {
    // console.log(errors, isValid);

    props.currentFormIsValid(!isValid || !value ? false : true);
    props.setForm({
      ...props.form,
      name: value[0],
      siret: value[1],
      animals: value[2],
      species: value[3],
    });
  }, [value, isValid]);

  const handleDelete = (i: number) => {
    setValue(
      "species",
      value[3].filter((species: string, index: number) => index !== i)
    );
  };

  const handleAddition = (tag: { id: string; text: string }) => {
    // Check if the tag's text is a valid animal species
    if (AnimalSpecies[tag.text as keyof typeof AnimalSpecies]) {
      setValue("species", [...value[3], tag.text]);
    }
  };

  return (
    <div className="flex flex-col gap-6">
      <div className="input">
        <p>Nom de l'association</p>
        <ErrorInput
          {...register("name")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="text"
          error={errors.name}
          required={true}
        />
      </div>
      <div className="input">
        <p>Numéro de SIREN</p>
        <ErrorInput
          {...register("siret")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="text"
          error={errors.siret}
          required={true}
        />
      </div>
      <div className="input">
        <p>Moyenne d'animaux disponibles par mois</p>
        <ErrorInput
          {...register("animals")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="text"
          error={errors.animals}
          required={true}
        />
      </div>
      <div className="input">
        <p>Principales espèces</p>
        <div className="w-auto">
          <ReactTags
            tags={value[3].map((species: string) => {
              return {
                id: species,
                text: species,
              };
            })}
            inputFieldPosition="bottom"
            autofocus={false}
            suggestions={suggestions}
            delimiters={delimiters}
            handleDelete={handleDelete}
            handleAddition={handleAddition}
            autocomplete
            classNames={{
              tags: "w-full mt-4",
              tag: "inline-block py-1 px-2 border-solid border-2 border-lightgray-200 rounded-md",
              tagInput: "w-full",
              tagInputField:
                "bg-primary-100 border-primary-200 mt-4 flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50",
              selected: "",
              remove: "ml-1 text-gray-400 ",
              suggestions: "w-full bg-slate-50 p-2 rounded-md shadow-md",
              activeSuggestion: "border-orange-300 bg-orange-100",
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default AssoFormStepOne;
