export interface Form {
  name: string;
  siret: string;
  animals: string;
  species: string[];
  address: string;
  addressNumber: string;
  zipCode: string;
  phone: string;
  city: string;
  country: string;
  email: string;
  password: string;
  confirmPassword: string;
  conscentCgu: boolean;
  newsletter: boolean;
}
