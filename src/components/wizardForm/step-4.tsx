import React, { useEffect, useState } from "react";
import { Input } from "../ui/input";
import * as yup from "yup";
import { Form } from "./form";
import { useForm, useWatch } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

// Yup validation schema
const schema = yup.object().shape({
  conscentCgu: yup.boolean().oneOf([true], "Ce champ est requis."),
  newsletter: yup.boolean().oneOf([true, false], "Ce champ est optionnel."),
});

const AssoFormStepFour = (props: {
  form: Form;
  setForm: any;
  currentFormIsValid: any;
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
    reset,
    control,
    watch,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
    defaultValues: {
      conscentCgu: props.form.conscentCgu,
      newsletter: props.form.newsletter,
    },
  });

  let value = useWatch({
    control,
    name: ["conscentCgu", "newsletter"],
  });

  useEffect(() => {
    props.currentFormIsValid(!isValid || !value ? false : true);
    props.setForm({
      ...props.form,
      conscentCgu: value[0],
      newsletter: value[1],
    });
  }, [value, isValid]);

  return (
    <div className="flex flex-col gap-4">
      <div>
        <h4 className="text-xl font-bold">Informations</h4>
        <p className="text-base my-2">
          Nom de l'association : {props.form.name}
        </p>
        <p className="text-base my-2">SIREN : {props.form.siret}</p>
      </div>
      <div>
        <h4 className="text-xl font-bold">Adresse</h4>
        <p className="text-base my-2">
          {props.form.addressNumber} {props.form.address}, {props.form.zipCode}{" "}
          {props.form.city}, {props.form.country}
        </p>
      </div>
      <div>
        <h4 className="text-xl font-bold">Compte</h4>
        <p className="text-base my-2">Email : {props.form.email}</p>
      </div>
      <div>
        <h3 className="text-xl font-bold">
          Conditions générales d'utilisations
        </h3>
        <div className="flex items-center">
          <Input
            {...register("conscentCgu")}
            type="checkbox"
            className="w-5 mr-2"
          />
          <label className="text-base">
            J'accepte les{" "}
            <a href="#" className="text-info-400">
              conditions d'utilisations
            </a>{" "}
            *
          </label>
        </div>
        <div className="flex items-center">
          <Input
            {...register("newsletter")}
            type="checkbox"
            className="w-5 mr-2"
          />
          <label className="text-base">
            Je souhaite recevoir par notification mail les nouveautés relatives
            a Animadopte
          </label>
        </div>
      </div>
    </div>
  );
};

export default AssoFormStepFour;
