import React, { useEffect } from "react";
import * as yup from "yup";
import { Form } from "./form";
import { useForm, useWatch } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import ErrorInput from "../errorInput/errorInput";

// Yup validation schema
const schema = yup.object().shape({
  address: yup.string().min(2).max(20).required("Address is required"),
  addressNumber: yup
    .string()
    .min(2)
    .max(20)
    .required("AddressNumber is required"),
  zipCode: yup.string().min(5).max(5).required("Zip code is required"),
  city: yup.string().min(1).max(20).required("City is required"),
  country: yup.string().min(1).max(20).required("Country is required"),
});

const AssoFormStepTwo = (props: {
  setForm: any;
  form: Form;
  currentFormIsValid: any;
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
    reset,
    control,
    watch,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
    defaultValues: {
      address: props.form.address,
      addressNumber: props.form.addressNumber,
      zipCode: props.form.zipCode,
      city: props.form.city,
      country: props.form.country,
    },
  });

  let value = useWatch({
    control,
    name: ["address", "addressNumber", "zipCode", "city", "country"],
  });

  useEffect(() => {
    props.currentFormIsValid(!isValid || !value ? false : true);
    props.setForm({
      ...props.form,
      address: value[0],
      addressNumber: value[1],
      zipCode: value[2],
      city: value[3],
      country: value[4],
    });
  }, [value, isValid]);

  return (
    <div className="flex flex-col gap-6">
      <div className="flex flex-row gap-4">
        <div className="input">
          <p>Numéro</p>
          <ErrorInput
            {...register("addressNumber")}
            className="bg-primary-100 border-primary-200 mt-2"
            type="text"
            error={errors.addressNumber}
            required={true}
          />
        </div>
        <div className="input columns-sm">
          <p>Rue</p>
          <ErrorInput
            {...register("address")}
            className="bg-primary-100 border-primary-200 mt-2"
            type="text"
            error={errors.address}
            required={true}
          />
        </div>
      </div>
      <div className="input">
        <p>Ville</p>
        <ErrorInput
          {...register("city")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="text"
          error={errors.city}
          required={true}
        />
      </div>
      <div className="input">
        <p>Code Postal</p>
        <ErrorInput
          {...register("zipCode")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="text"
          error={errors.zipCode}
          required={true}
        />
      </div>
      <div className="input">
        <p>Pays</p>
        <ErrorInput
          {...register("country")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="text"
          error={errors.country}
          required={true}
        />
      </div>
    </div>
  );
};

export default AssoFormStepTwo;
