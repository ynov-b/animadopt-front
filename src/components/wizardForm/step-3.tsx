import React, { useEffect } from "react";
import { Input } from "../ui/input";
import * as yup from "yup";
import { Form } from "./form";
import { useForm, useWatch } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import ErrorInput from "../errorInput/errorInput";

// Yup validation schema
const schema = yup.object().shape({
  email: yup.string().email().required("Email is required"),
  phone: yup.string().min(10).max(10).required("Phone is required"),
  password: yup.string().min(8).max(20).required("Password is required"),
  confirmPassword: yup
    .string()
    .min(8)
    .max(20)
    .required("Confirm password is required"),
});

const AssoFormStepThree = (props: {
  setForm: any;
  form: Form;
  currentFormIsValid: any;
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
    reset,
    control,
    watch,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
    defaultValues: {
      email: props.form.email,
      phone: props.form.phone,
      password: props.form.password,
      confirmPassword: props.form.confirmPassword,
    },
  });

  let value = useWatch({
    control,
    name: ["email", "phone", "password", "confirmPassword"],
  });

  useEffect(() => {
    props.currentFormIsValid(!isValid || !value ? false : true);
    props.setForm({
      ...props.form,
      email: value[0],
      phone: value[1],
      password: value[2],
      confirmPassword: value[3],
    });
  }, [value, isValid]);

  return (
    <div className="flex flex-col gap-6">
      <div className="input">
        <p>Email</p>
        <ErrorInput
          {...register("email")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="text"
          error={errors.email}
          required={true}
        />
      </div>
      <div className="input">
        <p>Téléphone</p>
        <ErrorInput
          {...register("phone")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="text"
          error={errors.phone}
          required={true}
        />
      </div>
      <div className="input">
        <p>Mot de passe</p>
        <ErrorInput
          {...register("password")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="password"
          error={errors.password}
          required={true}
        />
      </div>
      <div className="input">
        <p>Confirmer le mot de passe</p>
        <ErrorInput
          {...register("confirmPassword")}
          className="bg-primary-100 border-primary-200 mt-2"
          type="password"
          error={errors.confirmPassword}
          required={true}
        />
      </div>
    </div>
  );
};

export default AssoFormStepThree;
