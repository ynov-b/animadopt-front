"use client";

import { buttonVariants } from "@/components/ui/button";
import {
  Card,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import Link from "next/link";
import React from "react";
const Tab = () => {
  return (
    <Tabs defaultValue="user" className="min-h-screen w-full">
      <TabsList className="grid grid-cols-1 lg:grid-cols-2 p-0 h-auto">
        <TabsTrigger
          value="user"
          className="lg:text-3xl text-xl data-[state=active]:bg-orange-400 data-[state=inactive]:bg-gray-200 p-6"
        >
          Je suis un particulier
        </TabsTrigger>
        <TabsTrigger
          value="assoc"
          className="lg:text-3xl text-xl data-[state=active]:bg-gradient-end data-[state=inactive]:bg-gray-200 p-6"
        >
          Je suis une association
        </TabsTrigger>
      </TabsList>

      <TabsContent value="user" className="bg-orange-200">
        <div className="grid gap-7 grid-cols-1 lg:grid-cols-2 py-10">
          <Card className="text-orange-600 w-2/3 bg-white rounded justify-self-center">
            <CardHeader className="pb-7">
              <CardTitle className="lg:text-2xl text-base text-center pb-7">
                Je consulte les animaux disponible à l'adoption
              </CardTitle>
              <CardDescription>
                <img
                  src="/images/animals_logo.png"
                  alt="turtle"
                  className="w-1/3 m-auto"
                />
              </CardDescription>
            </CardHeader>
            <CardFooter className="justify-center">
              <Link
                className={`hover:bg-orange-600 text-orange-600 hover:text-white font-bold py-2 px-4 rounded ${buttonVariants(
                  { variant: "outline" },
                )}`}
                href="/animal/list"
              >
                Consulter
              </Link>
            </CardFooter>
          </Card>

          <Card className="text-orange-600 w-2/3 bg-white rounded flex flex-col justify-around justify-self-center">
            <CardHeader className="pb-7">
              <CardTitle className="lg:text-2xl text-base text-center pb-7">
                Je me connecte à mon profil d'adopteur
              </CardTitle>
              <CardDescription>
                <img
                  src="/images/user_logo.png"
                  alt="user_logo"
                  className="w-1/3 m-auto"
                />
              </CardDescription>
            </CardHeader>
            <CardFooter className="justify-center">
              <Link
                className={`hover:bg-orange-600 text-orange-600 hover:text-white font-bold py-2 px-4 rounded ${buttonVariants(
                  { variant: "outline" },
                )}`}
                href=""
              >
                Se connecter
              </Link>{" "}
            </CardFooter>
          </Card>
        </div>
      </TabsContent>

      <TabsContent value="assoc" className="bg-red-300">
        <div className="grid gap-7 grid-cols-1 lg:grid-cols-2 py-10">
          <Card className="text-red-500 w-2/3 bg-white rounded justify-self-center">
            <CardHeader className="pb-7">
              <CardTitle className="lg:text-2xl text-base text-center pb-7">
                Je veux inscrire mon association
              </CardTitle>
              <CardDescription>
                <img
                  src="/images/assoc.png"
                  alt="turtle"
                  className="w-1/3 m-auto "
                />
              </CardDescription>
            </CardHeader>
            <CardFooter className="justify-center">
              <Link
                className={`hover:bg-gradient-end text-gradient-end hover:text-white font-bold py-2 px-4 rounded ${buttonVariants(
                  { variant: "outline" },
                )}`}
                href="/auth/association/register"
              >
                S'inscrire
              </Link>
            </CardFooter>
          </Card>

          <Card className="text-red-500 w-2/3 bg-white rounded flex flex-col justify-around justify-self-center">
            <CardHeader className="pb-7">
              <CardTitle className="lg:text-2xl text-base text-center pb-7">
                J'accède à mon espace bénévole
              </CardTitle>
              <CardDescription>
                <img
                  src="/images/volonteer_logo.png"
                  alt="user_logo"
                  className="w-1/3 m-auto"
                />
              </CardDescription>
            </CardHeader>
            <CardFooter className="justify-center">
              <Link
                className={`hover:bg-red-500 text-gradient-end hover:text-white font-bold py-2 px-4 rounded ${buttonVariants(
                  { variant: "outline" },
                )}`}
                href="/auth/user/login"
              >
                Se connecter
              </Link>{" "}
            </CardFooter>
          </Card>
        </div>
      </TabsContent>
    </Tabs>
  );
};

export default Tab;
