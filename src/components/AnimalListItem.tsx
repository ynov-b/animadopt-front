import {
  Card,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import React from "react";
import Link from "next/link";
import { buttonVariants } from "@/components/ui/button";
import { capitalize } from "@/utils/capitalize";

interface Props {
  animal: Animal;
  onClick: (animal: Animal) => void;
}

const AnimalListItem: React.FC<Props> = ({ animal, onClick }) => {
  return (
    <Card
      className="bg-orange-600 hover:bg-orange-100 hover:text-orange-700 hover:border cursor-pointer text-orange-100 border-none mb-3"
      onClick={() => onClick(animal)}
    >
      <CardHeader className="grid grid-cols-2">
        <div className="justify-self-start">
          <CardTitle className="text-lg">{capitalize(animal.name)}</CardTitle>
          <CardDescription className="italic">{animal.species}</CardDescription>
        </div>

        <div className="justify-self-end">
          <img
            src={`${animal.picture ? animal.picture : "/images/moufette.jpg"} `}
            alt="animal-image"
            className="w-36 rounded-full"
          />
        </div>
      </CardHeader>

      {/* Display this button only for min-screen to redirect to [id] page */}
      <Link
        href={`/animal/${animal.animalId}`}
        className={`ml-5 py-2 px-4 rounded lg:hidden ${buttonVariants({
          variant: "outline",
        })}`}
      >
        Voir le détail
      </Link>

      <CardFooter className="flex justify-between"></CardFooter>
    </Card>
  );
};

export default AnimalListItem;
