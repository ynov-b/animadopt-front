import AnimalListItem from "@/components/AnimalListItem";
import AnimalDetail from "@/components/AnimalDetail";
import React, { FC, useEffect, useState } from "react";
import { Pagination } from "@/components/Pagination";

type AnimalListProps = {
  animals: Animal[] | undefined;
  filters: Partial<Record<string, string>>;
  onFilterChange: (filter: AnimalFilterType, callback?: () => void) => void;
};

export const AnimalList: FC<AnimalListProps> = ({ animals, filters }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedAnimal, setSelectedAnimal] = useState<Animal | null>(null);
  const itemsPerPage = 5;
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;

  const filteredAnimals = animals?.filter((animal: Animal) => {
    const speciesMatch = !filters.species || animal.species === filters.species;
    const breedMatch = !filters.breed || animal.breed === filters.breed;
    const sexMatch = !filters.sex || animal.sex === filters.sex;
    const associationMatch =
      !filters.association || animal.association_name === filters.association;

    return speciesMatch && breedMatch && sexMatch && associationMatch;
  });

  useEffect(() => {
    if (filteredAnimals!.length > 0) {
      const currentSelectedAnimal = filteredAnimals!.find(
        (animal) => animal.animalId === selectedAnimal?.animalId,
      );

      if (!currentSelectedAnimal) {
        setSelectedAnimal(filteredAnimals![0]);
      }
    }
  }, [filteredAnimals, selectedAnimal]);

  const paginateData =
    filteredAnimals && filteredAnimals.slice(indexOfFirstItem, indexOfLastItem);

  const handlePageChange = (selectedItem: any) => {
    setCurrentPage(selectedItem.selected + 1);
  };

  const handleHandleAnimalClick = (animal: Animal) => {
    setSelectedAnimal(animal);
  };

  return (
    <>
      <div className="italic text-gray-400 mb-5">
        {filteredAnimals!.length} résultat(s) trouvé(s)
      </div>
      <div className="grid lg:grid-cols-3 sm:grid-cols-1 gap-4 w-full mb-10">
        <div className="col-span-1">
          {paginateData && paginateData.length > 0 ? (
            paginateData.map((animal: Animal) => (
              <AnimalListItem
                key={animal.animalId}
                animal={animal}
                onClick={() => handleHandleAnimalClick(animal)}
              />
            ))
          ) : (
            <p>Aucun résultat</p>
          )}
        </div>

        <div className="col-span-2 hidden lg:block">
          {selectedAnimal && (
            <AnimalDetail
              animal={selectedAnimal}
              key={selectedAnimal.animalId}
            />
          )}
        </div>

        <Pagination
          totalItems={filteredAnimals!.length}
          itemsPerPage={itemsPerPage}
          currentPage={currentPage}
          onPageChange={handlePageChange}
        />
      </div>
    </>
  );
};
