"use client";

import React, { useState } from "react";
import { useRouter, usePathname } from "next/navigation";
import { Button } from "@/components/ui/button";
import Link from "next/link";

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };
  const router = useRouter();
  const pathName = usePathname();
  const redirect = () => {
    router.push(`/`);
  };

  return (
    <nav className="flex items-center justify-between flex-wrap bg-white w-full top-0 py-10 sticky mb-20">
      <div className="flex items-center flex-shrink-0 text-orange-600 mr-6">
        <a href="/">
          <img
            src="/images/logo-animadopt.png"
            alt="Mon Logo"
            className="w-6/12"
            onClick={redirect}
          />
        </a>
      </div>
      <div className="block lg:hidden">
        <button
          onClick={toggleMenu}
          className="flex items-center px-3 py-2 border rounded text-orange-600 border-orange-400 hover:text-orange-600 hover:border-white"
        >
          <svg
            className="fill-current h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>
      <div
        className={`${
          isOpen ? "block" : "hidden"
        } w-full lg:flex lg:items-center lg:w-auto`}
      >
        <div className="text-md lg:flex-grow lg:flex lg:justify-end">
          <a
            href="/animal/list"
            className={`block mt-4 lg:inline-block lg:mt-0 text-orange-600 ${
              pathName === "/animal/list" && "font-bold"
            } mr-8`}
          >
            Les animaux
          </a>
          <a
            href="/associations"
            className={`block mt-4 lg:inline-block lg:mt-0 text-orange-600 ${
              pathName === "/associations" && "font-bold"
            } mr-8`}
          >
            Les associations
          </a>
        </div>
        <div>
          <Button className="bg-orange-600 text-orange-100 font-bold hover:scale-105">
            <Link className="flex" href="/auth/user/login">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-5 h-5 mr-3"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z"
                />
              </svg>
              Se connecter
            </Link>
          </Button>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
