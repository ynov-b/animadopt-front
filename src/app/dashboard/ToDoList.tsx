"use client";

import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";

import React, { useState, useEffect, FormEvent } from "react";

interface Todo {
  text: string;
  completed: boolean;
}

const ToDoList = () => {
  const [toDo, setToDo] = useState<Todo[]>(() => {
    if (typeof window !== "undefined") {
      const savedTodos = localStorage.getItem("todos");
      if (savedTodos) {
        return JSON.parse(savedTodos);
      }
    }
    return [];
  });
  const [input, setInput] = useState("");

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(toDo));
  }, [toDo]);

  const addTodo = () => {
    if (input === "") return;
    setToDo([...toDo, { text: input, completed: false }]);
    setInput("");
  };

  const toggleTodo = (index: number) => {
    const newTodos = [...toDo];
    newTodos[index].completed = !newTodos[index].completed;
    setToDo(newTodos);
  };

  const removeTodo = (index: number) => {
    const newTodos = [...toDo];
    newTodos.splice(index, 1);
    setToDo(newTodos);
  };

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    addTodo();
  };

  return (
    <div>
      <form onSubmit={handleSubmit} className="flex flex-row gap-4">
        <Input value={input} onChange={(e) => setInput(e.target.value)} />
        <Button
          type="submit"
          className="bg-black text-white font-cairo font-bold mb-4"
        >
          Ajouter
        </Button>
      </form>
      <div>
        <ul className="overflow-y-auto">
          {toDo.map((item, index) => (
            <li key={index} className="flex gap-4 align-middle mb-4">
              <input
                type="checkbox"
                checked={item.completed}
                onChange={() => toggleTodo(index)}
              />
              <p
                className={
                  item.completed
                    ? "line-through text-slate-400 flex-grow cursor-pointer"
                    : "flex-grow cursor-pointer"
                }
                onClick={() => toggleTodo(index)}
              >
                {item.text}
              </p>
              <button
                className="self-end mr-3"
                onClick={() => removeTodo(index)}
              >
                Supprimer
              </button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default ToDoList;
