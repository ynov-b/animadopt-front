import React, { useMemo } from "react";
import { Calendar } from "@/components/ui/calendar";
import ToDoList from "./ToDoList";
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import RequestsCharts from "@/components/dashboard/RequestsCharts";
import Clock from "@/components/dashboard/Clock";

const Homepage = () => {
  // Calendar state
  const [date, setDate] = React.useState<Date | undefined>(new Date());

  const value1 = 1450;
  const value2 = 657;
  const value3 = 122;
  const value4 = 2151;

  return (
    <>
      <h1 className="font-montserrat font-bold text-3xl mb-8">Homepage</h1>
      <div className="flex flex-row space-x-8 flex-grow overflow-hidden">
        <div className="w-1/2">
          <div className="flex flex-col h-full">
            <div className="grid grid-cols-2 gap-8">
              <Card className="bg-white">
                <CardHeader>
                  <CardTitle className="font-normal">Adoptés</CardTitle>
                  <CardDescription className="text-slate-500">
                    Nombres d'animaux adoptés
                  </CardDescription>
                </CardHeader>
                <CardContent>
                  <p className="text-3xl font-bold">{value4}</p>
                </CardContent>
                <CardFooter>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6 text-green-600"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M2.25 18L9 11.25l4.306 4.307a11.95 11.95 0 015.814-5.519l2.74-1.22m0 0l-5.94-2.28m5.94 2.28l-2.28 5.941"
                    />
                  </svg>
                  <p className="ml-2">En hausse</p>
                </CardFooter>
              </Card>
              <Card className="bg-white">
                <CardHeader>
                  <CardTitle className="font-normal">En attente</CardTitle>
                  <CardDescription className="text-slate-500">
                    Nombres de requetes en attente
                  </CardDescription>
                </CardHeader>
                <CardContent>
                  <p className="text-3xl font-bold">{value1}</p>
                </CardContent>
                <CardFooter>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6 text-red-600"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M2.25 6L9 12.75l4.286-4.286a11.948 11.948 0 014.306 6.43l.776 2.898m0 0l3.182-5.511m-3.182 5.51l-5.511-3.181"
                    />
                  </svg>
                  <p className="ml-2">En baisse</p>
                </CardFooter>
              </Card>
              <Card className="bg-white">
                <CardHeader>
                  <CardTitle className="font-normal">Animaux</CardTitle>
                  <CardDescription className="text-slate-500">
                    Nombres d'animaux au total
                  </CardDescription>
                </CardHeader>
                <CardContent>
                  <p className="text-3xl font-bold">{value2}</p>
                </CardContent>
                <CardFooter>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6 text-red-600"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M2.25 6L9 12.75l4.286-4.286a11.948 11.948 0 014.306 6.43l.776 2.898m0 0l3.182-5.511m-3.182 5.51l-5.511-3.181"
                    />
                  </svg>
                  <p className="ml-2">En baisse</p>
                </CardFooter>
              </Card>
              <Card className="bg-white">
                <CardHeader>
                  <CardTitle className="font-normal">Traitement</CardTitle>
                  <CardDescription className="text-slate-500">
                    Temps moyen de traitement des requetes
                  </CardDescription>
                </CardHeader>
                <CardContent>
                  <p className="text-3xl font-bold">{value3}</p>
                </CardContent>
                <CardFooter>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6 text-green-600"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M2.25 18L9 11.25l4.306 4.307a11.95 11.95 0 015.814-5.519l2.74-1.22m0 0l-5.94-2.28m5.94 2.28l-2.28 5.941"
                    />
                  </svg>
                  <p className="ml-2">En hausse</p>
                </CardFooter>
              </Card>
            </div>
            <div className="flex-grow">
              <RequestsCharts />
            </div>
          </div>
        </div>
        <div className="w-1/2">
          <div className="flex h-fit gap-8">
            <Calendar
              mode="single"
              selected={date}
              onSelect={setDate}
              className="rounded-md border w-fit bg-white"
            />
            <div className="bg-gradient-end flex-grow h-auto rounded-md shadow-xl">
              <Clock />
            </div>
          </div>
          <div className="toDo mt-16">
            <h2 className="font-bold font-cairo mb-4">Liste des taches</h2>
            <ToDoList />
          </div>
        </div>
      </div>
    </>
  );
};

export default Homepage;
