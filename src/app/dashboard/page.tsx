"use client";

import DashboardNav from "@/components/DashboardNav";
import React, { useState } from "react";
import Homepage from "./Homepage";
import AssociationProfilePage from "@/components/dashboard/Profile";
import RequestList from "@/components/dashboard/RequestList";
import AnimalsList from "@/components/dashboard/AnimalsList";

const Dashboard = () => {
  const [activeTab, setActiveTab] = useState("Vue globale");

  return (
    <div className="flex flex-row p-3 h-screen bg-gradient-end">
      <DashboardNav
        className="p-8 flex flex-col mr-10"
        setActiveTab={setActiveTab}
        activeTab={activeTab}
      />
      <div className="p-8 bg-slate-50 text-black rounded-3xl shadow-2xl flex-grow flex flex-col">
        {(activeTab === "Vue globale" && <Homepage />) ||
          (activeTab === "Demandes" && (
            // Remplacer le contenu de cette div par le composant Demandes
            <div>
              <h1>Demandes</h1>
              <RequestList />
            </div>
          )) ||
          (activeTab === "Animaux" && (
            // Remplacer le contenu de cette div par le composant Animaux
            <div>
              <h1>Animaux</h1>
              <AnimalsList />
            </div>
          )) ||
          (activeTab === "Profil" && (
            // Remplacer le contenu de cette div par le composant Profil
            <AssociationProfilePage />
          ))}
      </div>
    </div>
  );
};

export default Dashboard;
