"use client";

import styles from "./styles.module.css";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import Illustration from "../../../../../public/images/undraw_welcome_cats_thqn.svg";
import "../../../globals.css";
import { Button } from "@/components/ui/button";
import AssoFormStepOne from "@/components/wizardForm/step-1";
import AssoFormStepTwo from "@/components/wizardForm/step-2";
import AssoFormStepThree from "@/components/wizardForm/step-3";
import AssoFormStepFour from "@/components/wizardForm/step-4";
import {redirect, useRouter} from "next/navigation";

const AuthAssociationRegisterPage = () => {
  // Step number
  const [step, setStep] = useState(1);
  // Form data
  const [form, setForm] = useState({
    name: "",
    siret: "",
    animals: "",
    country: "",
    species: [],
    addressNumber: "",
    address: "",
    zipCode: "",
    city: "",
    phone: "",
    email: "",
    password: "",
    confirmPassword: "",
    conscentCgu: false,
    newsletter: false,
  });

  // Manage the steps and the button status
  const [isEnable, setIsEnable] = useState(false);
  const [step1IsValid, setStep1IsValid] = useState(false);
  const [step2IsValid, setStep2IsValid] = useState(false);
  const [step3IsValid, setStep3IsValid] = useState(false);
  const [step4IsValid, setStep4IsValid] = useState(false);

  useEffect(() => {
    console.log("FORM DATA :", form);

    switch (step) {
      case 1:
        setIsEnable(step1IsValid);
        break;
      case 2:
        setIsEnable(step2IsValid);
        break;
      case 3:
        setIsEnable(step3IsValid);
        break;
      case 4:
        setIsEnable(step4IsValid);
        break;
      default:
        setIsEnable(false);
    }
  }, [step, step1IsValid, step2IsValid, step3IsValid, step4IsValid]);

  return (
    <div
      id={styles.authAssociationRegisterPage}
      className="flex min-h-screen p-10"
    >
      <div className="columns-xl">
        <form className="p-16 bg-white rounded-3xl shadow-2xl flex flex-col min-h-full text-gray-600">
          <h1 id={styles.authAssociationRegisterPage__h1}>Bienvenue</h1>
          <h3 id={styles.authAssociationRegisterPage__h3}>
            Inscrivez votre asssocation
          </h3>
          <div className={styles.authAssociationRegisterPage__progress}>
            <div className={styles.element}>
              <h2 className={step == 1 ? styles.active : ""}>1</h2>
              <p className={step == 1 ? styles.active : ""}>Informations</p>
            </div>
            <div className={styles.element}>
              <h2 className={step == 2 ? styles.active : ""}>2</h2>
              <p className={step == 2 ? styles.active : ""}>Adresses</p>
            </div>
            <div className={styles.element}>
              <h2 className={step == 3 ? styles.active : ""}>3</h2>
              <p className={step == 3 ? styles.active : ""}>Compte</p>
            </div>
            <div className={styles.element}>
              <h2 className={step == 4 ? styles.active : ""}>4</h2>
              <p className={step == 4 ? styles.active : ""}>Validation</p>
            </div>
          </div>
          <div className="flex-grow">
            {step === 1 && (
              <AssoFormStepOne
                setForm={setForm}
                form={form}
                currentFormIsValid={setStep1IsValid}
              />
            )}
            {step === 2 && (
              <AssoFormStepTwo
                setForm={setForm}
                form={form}
                currentFormIsValid={setStep2IsValid}
              />
            )}
            {step === 3 && (
              <AssoFormStepThree
                setForm={setForm}
                form={form}
                currentFormIsValid={setStep3IsValid}
              />
            )}
            {step === 4 && (
              <AssoFormStepFour
                setForm={setForm}
                form={form}
                currentFormIsValid={setStep4IsValid}
              />
            )}
          </div>
          <div className="buttons place-self-end">
            <Button
              className="bg-info-100 text-info-400 px-10 mr-2"
              disabled={step === 1}
              onClick={(e) => {
                e.preventDefault();
                step > 1 && setStep(step - 1);
              }}
            >
              Précedent
            </Button>
            {step !== 4 && (
              <Button
                className="bg-info-400 text-white px-10"
                onClick={(e) => {
                  e.preventDefault();
                  step < 4 && setStep(step + 1);
                }}
                disabled={isEnable ? false : true}
              >
                Suivant
              </Button>
            )}
            {step === 4 && (
              <Button
                type="submit"
                className="bg-green-500 text-white px-10"
                disabled={isEnable ? false : true}
                onClick={() => redirect("https://www.animadopt.fr/dashboard")}
              >
                Valider
              </Button>
            )}
          </div>
        </form>
      </div>
      <div className="columns-auto flex grow place-content-center">
        <Image src={Illustration} alt="Illustration of Cats" />
      </div>
    </div>
  );
};

export default AuthAssociationRegisterPage;
