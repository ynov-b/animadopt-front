"use client";
import { useState, FormEvent } from "react";
import styles from "./styles.module.css";
import Image, { StaticImageData } from "next/image";
import Illustration from "../../../../../public/images/undraw_welcome_cats_thqn.svg";
import { Button } from "@/components/ui/button";

const LoginPage: React.FC = () => {
    // Form data
    const [form, setForm] = useState({
      email: "",
      pwd: "",
    });

    interface ApiResponse {
      message: string;
      code: number;
    }
  
    // State for error message
    const [error, setError] = useState<string | null>(null);
  
    const handleLogin = async (e: FormEvent) => {
      e.preventDefault();
      setError(null);
    
      try {
        const response = await fetch("https://api.animadopt.fr/api/login", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(form),
        });
    
        if (response.ok) {
          // Login successful, you can redirect or perform other actions
          console.log("Login successful");
          setError(null); // Reset error state on success
          setError(`Connecté`);
        } else {
          // Handle login failure
          const data = await response.json() as ApiResponse;
          if (data?.message) {
            setError(`Erreur : ${data.message}`);
            console.error("Login failed:", data.message);
          } else {
            setError("Erreur: vérifiez vos identifiants.");
            console.error("Unknown error during login.");
          }
        }
      } catch (error) {
        setError("Une erreur s'est produite lors de la connexion.");
        console.error("Error during login:", error);
      }
    };
    
    

  return (
    <div id={styles.authAssociationRegisterPage} className="flex min-h-screen p-10">
      <div className="columns-xl">
        <form
          className="p-16 bg-white rounded-3xl shadow-2xl flex flex-col min-h-full"
          onSubmit={(e) => {
            handleLogin(e).catch((error) => console.error("Error during login:", error));
          }}
        >
          <h1 id={styles.authAssociationRegisterPage__h1}>Connexion</h1>
          <h3 id={styles.authAssociationRegisterPage__h3}>Entrez vos identifiants</h3>
          <div className="flex flex-col flex-grow justify-center gap-5">
            <div className="mb-4">
              <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                Email
              </label>
              <input
                type="email"
                id="email"
                name="email"
                value={form.email}
                onChange={(e) => setForm({ ...form, email: e.target.value })}
                className="mt-1 p-2 w-full border rounded-md text-black"
              />
            </div>
            <div className="mb-8">
              <label htmlFor="password" className="block text-sm font-medium text-gray-700">
                Mot de passe
              </label>
              <input
                type="password"
                id="password"
                name="password"
                value={form.pwd}
                onChange={(e) => setForm({ ...form, pwd: e.target.value })}
                className="mt-1 p-2 w-full border rounded-md text-black"
              />
            </div>
            {error && <div className="error-message text-red-500">{error}</div>}
            <Button
              type="submit"
              className="bg-green-500 text-white px-10 w-48 ml-auto hover:bg-green-300"
            >
              Se connecter
            </Button>
          </div>
          <div className="buttons place-self-end mt-5"></div>
        </form>
      </div>
      <div className="columns-auto flex grow place-content-center">
        <Image src={Illustration as StaticImageData} alt="Illustration of Cats" />
      </div>
    </div>
  );
};

export default LoginPage;
