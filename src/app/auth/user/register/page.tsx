"use client"

import { useState, FormEvent } from "react";
import * as React from "react";
import styles from "./styles.module.css";
import Image, { StaticImageData } from "next/image";
import Illustration from "../../../../../public/images/undraw_welcome_cats_thqn.svg";
import { Checkbox } from "@/components/ui/checkbox"
import { Button } from "@/components/ui/button"
const RegisterPage: React.FC = () => {

  const [date, setDate] = React.useState<Date>()
  const [form, setForm] = useState({
    email: "",
    pwd: "",
    firstname: "",
    lastname: "",
    gender: "MALE",
    birthday: "2003-01-01",
    habitat: "HOUSE",
    cguSigned: 1,
    phone: "0839238494",
    nbAnimals: 3,
  });

  const [error, setError] = useState<string | null>(null);

  const handleRegister = async (e: FormEvent) => {
    e.preventDefault();
    setError(null);

    try {
      const response = await fetch("https://api.animadopt.fr/api/users/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(form),
      });

      if (response.ok) {
        console.log("Registration successful");
        setError(null);
        setError(`Inscrit`);
      } else {
        const data = await response.json();
        if (data?.message) {
          setError(`Erreur : ${data.message}`);
          console.error("Registration failed:", data.message);
        } else {
          setError("Erreur: vérifiez vos informations d'inscription.");
          console.error("Unknown error during registration.");
        }
      }
    } catch (error) {
      setError("Une erreur s'est produite lors de l'inscription.");
      console.error("Error during registration:", error);
    }
  };

  return (
    <div id={styles.authAssociationRegisterPage} className="flex min-h-screen p-10">
      <div className="columns-xl">
        <form
          className="p-16 bg-white rounded-3xl shadow-2xl flex flex-col min-h-full"
          onSubmit={(e) => {
            handleRegister(e).catch((error) => console.error("Error during registration:", error));
          }}
        >
          <h1 id={styles.authAssociationRegisterPage__h1}>Inscription</h1>
          <h3 id={styles.authAssociationRegisterPage__h3}>Créez votre compte</h3>
          <div className="flex flex-col flex-grow justify-center gap-5">
            <div className="flex flex-row gap-10">
              <div className="mb-4">
                <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                  Nom
                </label>
                <input
                  type="text"
                  id="lastname"
                  name="lastname"
                  value={form.lastname}
                  onChange={(e) => setForm({ ...form, lastname: e.target.value })}
                  className="mt-1 p-2 w-full border rounded-md text-black"
                />
              </div>
              <div className="mb-4">
                <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                  Prénom
                </label>
                <input
                  type="text"
                  id="firstname"
                  name="firstname"
                  value={form.firstname}
                  onChange={(e) => setForm({ ...form, firstname: e.target.value })}
                  className="mt-1 p-2 w-full border rounded-md text-black"
                />
              </div>
            </div>
            <div className="mb-4">
              <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                Email
              </label>
              <input
                type="email"
                id="email"
                name="email"
                value={form.email}
                onChange={(e) => setForm({ ...form, email: e.target.value })}
                className="mt-1 p-2 w-full border rounded-md text-black"
              />
            </div>
            <div className="mb-4">
              <label htmlFor="pwd" className="block text-sm font-medium text-gray-700">
                Mot de passe
              </label>
              <input
                type="password"
                id="pwd"
                name="pwd"
                value={form.pwd}
                onChange={(e) => setForm({ ...form, pwd: e.target.value })}
                className="mt-1 p-2 w-full border rounded-md text-black"
              />
            </div>
            <div className="flex items-center space-x-2">
              <Checkbox id="terms"/>
              <label
                htmlFor="terms"
                className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
              >
                J'accepte les CGU
              </label>
            </div>
            {error && <div className="error-message text-red-500">{error}</div>}
            <Button
              type="submit"
              className="bg-green-500 text-white px-10 w-48 ml-auto hover:bg-green-300"
            >
              S'inscrire
            </Button>
          </div>
          <div className="buttons place-self-end mt-5"></div>
        </form>
      </div>
      <div className="columns-auto flex grow place-content-center">
        <Image src={Illustration as StaticImageData} alt="Illustration of Cats" />
      </div>
    </div>
  );
};

export default RegisterPage;
