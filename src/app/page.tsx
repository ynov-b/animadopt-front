"use client";

import Tab from "@/components/Tab";

export default function Home() {
  return (
    <>
      <div className="text-orange-600 grid lg:grid-cols-2 grid-cols-1 py-10 w-full bg-cover h-auto">
        <div>
          <h1 className="text-4xl pb-7">Adoptez l'amour inconditionnel.</h1>

          <p className="text-xl italic">
            Trouvez votre compagnon idéal sur Animadop't. <br />
            Facilitez l'adoption grâce à nos associations dévouées.
          </p>
        </div>
        <div>
          <img src="/images/banner-logo.jpg" alt="banner_logo" />
        </div>
      </div>
      <Tab />
    </>
  );
}
