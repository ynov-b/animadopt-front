"use client";

import React, { useState } from "react";
import { useQuery } from "@tanstack/react-query";
import { apiGetAnimals } from "@/shared/api/animals";
import { AnimalFilter } from "@/components/AnimalFilter";
import { AnimalList } from "@/components/AnimalList";

export default function AnimalListPage(): React.JSX.Element {
  const {
    data: animalsFromApi,
    isLoading,
    isError,
  } = useQuery({ queryKey: ["animals"], queryFn: apiGetAnimals });

  const [filters, setFilters] = useState<Partial<Record<string, string>>>({});

  const handleFilterChange = (filter: AnimalFilterType): void => {
    setFilters((prevFilters) => ({ ...prevFilters, ...filter }));
  };

  if (isLoading) {
    return (
      <div className="flex min-h-screen flex-col items-center bg-background mx-auto container">
        Chargement en cours...
      </div>
    );
  }

  if (isError) {
    return (
      <div>Une erreur s'est produite lors de la récupération des animaux</div>
    );
  }

  return (
    <>
      <AnimalFilter onFilterChange={handleFilterChange} />
      {animalsFromApi && animalsFromApi.length > 0 && (
        <AnimalList
          animals={animalsFromApi}
          filters={filters}
          onFilterChange={handleFilterChange}
        />
      )}
    </>
  );
}
