"use client";

import React from "react";
import { useParams } from "next/navigation";
import { calculateAge } from "@/utils/age";
import { capitalize } from "@/utils/capitalize";
import { useQuery } from "@tanstack/react-query";
import { apiGetAnimal } from "@/shared/api/animals";
import { Modal } from "@/components/Modal";
import { Badge } from "@/components/ui/badge";
import { Button } from "@/components/ui/button";

export default function Detail() {
  const params = useParams();
  const { id } = params;

  if (typeof id === "number") {
    const {
      data: animal,
      isLoading,
      isError,
    } = useQuery({
      queryKey: ["animalDetail"],
      queryFn: () => apiGetAnimal(id),
    });

    if (isLoading) {
      return <div>Chargement en cours...</div>;
    }

    if (animal) {
      return (
        <div className="grid text-orange-700 bg-orange-100 p-10 items-center gap-4">
          <p className="text-center font-bold">{animal.name}</p>
          <img
            src={`/images/moufette.jpg`}
            alt="animal-image"
            className="my-4 rounded-lg w-1/2 m-auto"
          />

          <div className={"flex justify-evenly my-5"}>
            <div>
              <p className={"font-bold text-center"}>Espèce</p>
              <div>
                <Badge className="bg-orange-200"> {animal.species}</Badge>
              </div>
            </div>
            <div>
              <p className={"font-bold text-center"}>Race</p>

              <div>
                <Badge className="bg-orange-200"> {animal.breed}</Badge>
              </div>
            </div>
            <div>
              <p className={"font-bold text-center"}>Sexe</p>
              <div>
                <Badge className="bg-orange-200">{animal.sex}</Badge>
              </div>
            </div>
            <div>
              <p className={"font-bold text-center"}>Couleur</p>
              <div>
                <Badge className="bg-orange-200">{animal.color}</Badge>
              </div>
            </div>
          </div>

          <div className="flex mt-10">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-5 h-5 mr-3"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z"
              />
            </svg>

            <span className=" mr-2">Je suis né(e) le</span>
            <span className="font-bold">{animal.birthday}</span>
          </div>

          <div className="flex">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-5 h-5 mr-3"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M12 3v17.25m0 0c-1.472 0-2.882.265-4.185.75M12 20.25c1.472 0 2.882.265 4.185.75M18.75 4.97A48.416 48.416 0 0012 4.5c-2.291 0-4.545.16-6.75.47m13.5 0c1.01.143 2.01.317 3 .52m-3-.52l2.62 10.726c.122.499-.106 1.028-.589 1.202a5.988 5.988 0 01-2.031.352 5.988 5.988 0 01-2.031-.352c-.483-.174-.711-.703-.59-1.202L18.75 4.971zm-16.5.52c.99-.203 1.99-.377 3-.52m0 0l2.62 10.726c.122.499-.106 1.028-.589 1.202a5.989 5.989 0 01-2.031.352 5.989 5.989 0 01-2.031-.352c-.483-.174-.711-.703-.59-1.202L5.25 4.971z"
              />
            </svg>

            <span className=" mr-2">Je pèse</span>
            <span className="font-bold">{animal.weight} kg</span>
          </div>

          <div className="flex">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-5 h-5 mr-3"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M3.75 13.5l10.5-11.25L12 10.5h8.25L9.75 21.75 12 13.5H3.75z"
              />
            </svg>
            <span className=" mr-2">On dit de moi que je suis plutôt</span>
            <span className="font-bold">{animal.mind}</span>
          </div>

          <div className="flex mb-4">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-5 h-5 mr-3"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M18 18.72a9.094 9.094 0 003.741-.479 3 3 0 00-4.682-2.72m.94 3.198l.001.031c0 .225-.012.447-.037.666A11.944 11.944 0 0112 21c-2.17 0-4.207-.576-5.963-1.584A6.062 6.062 0 016 18.719m12 0a5.971 5.971 0 00-.941-3.197m0 0A5.995 5.995 0 0012 12.75a5.995 5.995 0 00-5.058 2.772m0 0a3 3 0 00-4.681 2.72 8.986 8.986 0 003.74.477m.94-3.197a5.971 5.971 0 00-.94 3.197M15 6.75a3 3 0 11-6 0 3 3 0 016 0zm6 3a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0zm-13.5 0a2.25 2.25 0 11-4.5 0 2.25 2.25 0 014.5 0z"
              />
            </svg>
            <span className=" mr-2">Je recherche un-e humain-e </span>
            <span className="font-bold">{animal.adopterProfile}</span>
          </div>

          <div className="bg-orange-50 p-6 rounded-lg">"{animal.moreInfo}"</div>

          <Button className="bg-orange-200 text-orange-600 w-1/2 m-auto mt-6 hover:scale-105">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-5 h-5 mr-3"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M13.5 21v-7.5a.75.75 0 01.75-.75h3a.75.75 0 01.75.75V21m-4.5 0H2.36m11.14 0H18m0 0h3.64m-1.39 0V9.349m-16.5 11.65V9.35m0 0a3.001 3.001 0 003.75-.615A2.993 2.993 0 009.75 9.75c.896 0 1.7-.393 2.25-1.016a2.993 2.993 0 002.25 1.016c.896 0 1.7-.393 2.25-1.016a3.001 3.001 0 003.75.614m-16.5 0a3.004 3.004 0 01-.621-4.72L4.318 3.44A1.5 1.5 0 015.378 3h13.243a1.5 1.5 0 011.06.44l1.19 1.189a3 3 0 01-.621 4.72m-13.5 8.65h3.75a.75.75 0 00.75-.75V13.5a.75.75 0 00-.75-.75H6.75a.75.75 0 00-.75.75v3.75c0 .415.336.75.75.75z"
              />
            </svg>
            {animal.association_name}
          </Button>
          <Modal animal={animal} />
        </div>
      );
    }
  }

  return (
    <div>
      <h1>Animal non trouvé</h1>
    </div>
  );
}
