enum AnimalSpecies {
  Chien = "Chien",
  Chat = "Chat",
  Oiseau = "Oiseau",
  Poisson = "Poisson",
  Lapin = "Lapin",
  Hamster = "Hamster",
  Cobaye = "Cobaye",
  Serpent = "Serpent",
  Lézard = "Lézard",
  Tortue = "Tortue",
  Furet = "Furet",
  Cheval = "Cheval",
  Autre = "Autre",
}

export default AnimalSpecies;
