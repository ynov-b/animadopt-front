"use client";

import "./globals.css";
import { Montserrat } from "next/font/google";
import { Provider } from "@/app/provider";
import Navbar from "@/components/Navbar";
import Footer from "@/components/Footer";
import { usePathname } from "next/navigation";
import React from "react";
import { Toaster } from "@/components/ui/toaster";

const montserrat = Montserrat({
  weight: "400",
  subsets: ["latin"],
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const path = usePathname();

  return (
    <html lang="en" title="Animadop't">
      <body className={` ${montserrat.className}`}>
        {/* Display main layout with header/footer only for user website */}
        {path === "/dashboard" ||
        path === "/auth/association/register" ||
        path === "/auth/user/login" ||
        path === "/auth/user/register"
          ? (
          <Provider>{children}</Provider>
        ) : (
          <main className="flex min-h-screen flex-col items-center bg-white container">
            <Toaster />
            <Provider>
              <Navbar />
              {children}
              <Footer />
            </Provider>
          </main>
        )}
      </body>
    </html>
  );
}
