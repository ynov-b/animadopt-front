'use client'
import { useQuery } from "@tanstack/react-query";
import {apiGetAssociationId} from "@/shared/api/associations";
import {useParams, useRouter} from "next/navigation";
import {Params} from "next/dist/shared/lib/router/utils/route-matcher";
import {Card} from "@/components/ui/card";

interface AssociationsIdPageParams extends Params { id: string }

const AssociationDetails = () => {
  const params = useParams<AssociationsIdPageParams>()
  const router = useRouter()

  const id = +params.id

  const { data: association, isLoading, isError } =
      useQuery({queryKey: ["associationDetail"], queryFn: () => apiGetAssociationId(id)});


  const handleAnimalListClick = () => {
    router.push("/animal/list");
  };

  if (isLoading) {
    return <div>Chargement en cours...</div>;
  }

  if (isError) {
    return <div>Une erreur s'est produite lors de la récupération des détails de l'association</div>;
  }

  return (
    <div className="flex min-h-screen flex-col items-center bg-white mx-auto container">
      <div className="bg-white p-4 rounded-lg w-full max-w-screen-lg justify-center space-y-5">
        <h1 className="text-3xl font-semibold text-primary">{association?.name}</h1>
        <p className="text-gray-600">
          <b>Localisation:</b> {association?.localisation}
        </p>
        {association && (
          <div style={{ width: "100%", height: "400px" }}>
            <iframe
              width="100%"
              height="100%"
              src={`https://maps.google.com/maps?q=${encodeURIComponent(association.localisation)}&output=embed`}
              title="Location Map"
            />
          </div>
        )}
        <div className="text-gray-600 flex justify-between">
          <p><b>Email:</b> {association?.email}</p>
          <p><b>Téléphone:</b> {association?.phone}</p>
          <p><b>Nombre d'animaux:</b> {association?.nbPets}</p>
        </div>
        <div>
          <b>Liste des espèces:</b>
          <ul className={"flex gap-2 mt-2"}>
            {association?.speciesList.map((species, index) => (
              <Card className={"border-primary-350 text-orange-400 pr-2 pl-2"}>
                <li key={index}>{species}</li>
              </Card>
            ))}
          </ul>
        </div>
      </div>
      <div className={"bg-primary-500 hover:bg-orange-200 text-md px-4 py-2 leading-none border rounded text-white mt-4 lg:mt-0 font-bold drop-shadow-md"}>
        <button onClick={() => handleAnimalListClick()}>
          Voir Animaux
        </button>
      </div>
    </div>
  );
};

export default AssociationDetails;
