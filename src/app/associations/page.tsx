"use client";
import {
  Card,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { apiGetAssociations } from "@/shared/api/associations";
import { useQuery } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { Species } from "@/shared/enum/SpeciesEnum";
import { Badge } from "@/components/ui/badge";
const Associations = () => {
  const router = useRouter();
  const [sortByPetsAsc, setSortByPetsAsc] = useState(false);
  const [sortByPetsDesc, setSortByPetsDesc] = useState(false);
  const [sortAlphabetically, setSortAlphabetically] = useState(false);
  const [selectedSpecies, setSelectedSpecies] = useState<string | null>(null);

  const {
    data: associationsData,
    isLoading,
    isError,
  } = useQuery({ queryKey: ["associations"], queryFn: apiGetAssociations });

  const handleAssociationClick = (id: number) => {
    router.push(`/associations/${id}`);
  };

  let sortedAssociations = [...(associationsData ?? [])];

  if (sortByPetsAsc) {
    sortedAssociations = sortedAssociations.sort((a, b) => a.nbPets - b.nbPets);
  }
  if (sortByPetsDesc) {
    sortedAssociations = sortedAssociations.sort((a, b) => b.nbPets - a.nbPets);
  }
  if (sortAlphabetically) {
    sortedAssociations = sortedAssociations.sort((a, b) =>
      a.name.localeCompare(b.name),
    );
  }
  if (selectedSpecies) {
    sortedAssociations = sortedAssociations.filter((assoc) =>
      assoc.speciesList.includes(selectedSpecies),
    );
  }

  if (isLoading) {
    return (
      <div className="flex min-h-screen flex-col items-center bg-background mx-auto container">
        Chargement en cours...
      </div>
    );
  }

  if (isError) {
    return (
      <div>
        Une erreur s'est produite lors de la récupération des associations
      </div>
    );
  }

  return (
    <div className="flex min-h-screen flex-col items-center bg-background mx-auto container">
      <nav className="bg-primary-350 p-4 rounded-2xl">
        <div className="container mx-auto">
          <h1 className="text-4xl font-semibold text-white">Associations</h1>
        </div>
      </nav>
      <div className="py-10 container mx-auto">
        <div className=" p-4 w-full mb-10 bg-orange-50 border-2 rounded-lg border-orange-300">
          <div className="grid lg:grid-cols-12 gap-x-3">
            <label
              className="col-span-3 m-auto rounded-full cursor-pointer"
              data-ripple-dark="true"
            >
              <input
                type="checkbox"
                className="mt-2 before:content[''] peer relative h-4 w-4 cursor-pointer appearance-none rounded-md border border-orange-500 transition-all before:absolute before:top-2/4 before:left-2/4 before:block before:h-12 before:w-12 before:-translate-y-2/4 before:-translate-x-2/4 before:rounded-full before:bg-white before:opacity-0 before:transition-opacity checked:border-orange-500 checked:bg-orange-500 checked:before:bg-orange-500 hover:before:opacity-10"
                checked={sortByPetsAsc}
                onChange={() => {
                  setSortByPetsAsc(!sortByPetsAsc);
                  setSortByPetsDesc(false);
                  setSortAlphabetically(false);
                }}
              />
              <span className="m-auto text-black">{" Animaux 1<2"}</span>
            </label>
            <label
              className="col-span-3 m-auto rounded-full cursor-pointer"
              data-ripple-dark="true"
            >
              <input
                type="checkbox"
                className="mt-2 before:content[''] peer relative h-4 w-4 cursor-pointer appearance-none rounded-md border border-orange-500 transition-all before:absolute before:top-2/4 before:left-2/4 before:block before:h-12 before:w-12 before:-translate-y-2/4 before:-translate-x-2/4 before:rounded-full before:bg-white before:opacity-0 before:transition-opacity checked:border-orange-500 checked:bg-orange-500 checked:before:bg-orange-500 hover:before:opacity-10"
                checked={sortByPetsDesc}
                onChange={() => {
                  setSortByPetsDesc(!sortByPetsDesc);
                  setSortByPetsAsc(false);
                  setSortAlphabetically(false);
                }}
              />
              <span className="m-auto text-black">{" Animaux 2>1"}</span>
            </label>
            <label
              className="col-span-3 m-auto rounded-full cursor-pointer"
              data-ripple-dark="true"
            >
              <input
                type="checkbox"
                className="mt-2 before:content[''] peer relative h-4 w-4 cursor-pointer appearance-none rounded-md border border-orange-500 transition-all before:absolute before:top-2/4 before:left-2/4 before:block before:h-12 before:w-12 before:-translate-y-2/4 before:-translate-x-2/4 before:rounded-full before:bg-white before:opacity-0 before:transition-opacity checked:border-orange-500 checked:bg-orange-500 checked:before:bg-orange-500 hover:before:opacity-10"
                checked={sortAlphabetically}
                onChange={() => {
                  setSortAlphabetically(!sortAlphabetically);
                  setSortByPetsAsc(false);
                  setSortByPetsDesc(false);
                }}
              />
              <span className="m-auto text-black">{" A-Z"}</span>
            </label>
            <label className="col-span-3 m-auto">
              <select
                className="bg-orange-50 mb-2 text-black border-none"
                value={selectedSpecies ?? ""}
                onChange={(e) => setSelectedSpecies(e.target.value || null)}
              >
                <option value="">Toutes les espèces</option>
                {Object.values(Species).map((species) => (
                  <option key={species} value={species}>
                    {species}
                  </option>
                ))}
              </select>
            </label>
          </div>
        </div>
        {sortedAssociations !== undefined && (
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
            {sortedAssociations.map((assoc) => (
              <Card
                key={assoc.assoId}
                onClick={() => handleAssociationClick(assoc.assoId)}
                className="cursor-pointer transition duration-300 hover:scale-105 rounded-2xl"
              >
                <CardHeader className="p-4">
                  <CardTitle className="text-2xl font-semibold text-primary-500">
                    {assoc.name}
                  </CardTitle>
                  <CardDescription className="text-gray-600">
                    <b>Localisation:</b> {assoc.localisation}
                  </CardDescription>
                  <CardDescription className="text-gray-600">
                    <b>Email:</b> {assoc.email}
                  </CardDescription>
                  <CardDescription className="text-gray-600">
                    <b>Téléphone:</b> {assoc.phone}
                  </CardDescription>
                  <CardDescription className="text-gray-600">
                    <b>Nombre d'animaux:</b> {assoc.nbPets}
                  </CardDescription>
                  <CardDescription className="text-gray-600">
                    <b>Liste des espèces:</b>

                    {assoc.speciesList.map((species, index) => (
                      <Badge
                        className={
                          "border-primary-350 border-2 text-orange-400"
                        }
                        key={index}
                      >
                        {species}
                      </Badge>
                    ))}
                  </CardDescription>
                </CardHeader>
              </Card>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Associations;
