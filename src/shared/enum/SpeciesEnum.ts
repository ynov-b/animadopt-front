export enum Species {
  Chien = "Chien",
  Chat = "Chat",
  Oiseau = "Oiseau",
  Poisson = "Poisson",
  Wabbit = "Wabbit",
  Hamster = "Hamster",
  Serpent = "Serpent",
  Lezard = "Lezard",
  Tortue = "Tortue",
  Furret = "Furret",
  Cheval = "Cheval",
}
