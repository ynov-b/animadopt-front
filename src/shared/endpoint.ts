export const DOMAIN = "https://api.animadopt.fr";
// export const DOMAIN = "http://localhost:8080";

export const BASE_ENDPOINT = `${DOMAIN}/api`;

export const API_ENDPOINTS = {
  ASSOCIATIONS: {
    ASSOCIATIONS: `${BASE_ENDPOINT}/associations`,
    ASSOCIATIONS_ID: (assoId: number): string =>
      `${BASE_ENDPOINT}/associations/${assoId}`,
    ANIMALS_IN: (assoId: number): string =>
      `${BASE_ENDPOINT}/associations/${assoId}/animals`,
    REQUESTS_IN: (assoId: number): string =>
      `${BASE_ENDPOINT}/associations/${assoId}/requests`,
    CREATE_ANIMAL: (assoId: number) :string =>
      `${BASE_ENDPOINT}/associations/${assoId}/animals`
  },
  ANIMALS: {
    ANIMALS: `${BASE_ENDPOINT}/animals`,
    ADOPTION_REQUEST: (animalId: number) =>
      `${BASE_ENDPOINT}/animals/${animalId}/requests`,
    ANIMALS_ID: (animalId: number): string =>
      `${BASE_ENDPOINT}/animals/${animalId}`,
  },
  NOTES: {
    NOTES: `${BASE_ENDPOINT}/notes`,
    NOTES_ID: (noteId: number): string => `${BASE_ENDPOINT}/notes/${noteId}`,
  },
  ANIMAL_SPECIES: {
    ANIMAL_SPECIES: `${BASE_ENDPOINT}/animal-species`,
    SPECIES_ID: (speciesId: string): string =>
      `${BASE_ENDPOINT}/animal-species/${speciesId}`,
  },
};
