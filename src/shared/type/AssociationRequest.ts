interface AssociationRequest {
  assoId: number;
  association_name: string;
  animalId: number;
  animal_name: string;
  user_firstname: string;
  user_lastname: string;
  lastStatusUpdate: Date;
  creationDate: Date;
  requestId: number;
  userId: number;
  userMessage: string;
  status: string;
}

export default AssociationRequest;
