export interface AssociationType {
  assoId: number;
  name: string;
  localisation: string;
  email: string;
  phone: string;
  codeAsso: number;
  nbPets: number;
  speciesList: string[];
  animals: Animal[];
}
