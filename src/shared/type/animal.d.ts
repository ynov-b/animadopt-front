interface Animal {
  animalId: number;
  name: string;
  adopted: boolean;
  association_name: string;
  breed: string;
  picture: string;
  birthday: string;
  weight: number;
  mind: string;
  color: string;
  adopterProfile: string;
  species?: string;
  moreInfo: string;
  sex: string;
  speciesId: string;
  assoId: number;
}

interface AnimalFilterType {
  species: string;
  breed: string;
  sex: string;
  association: string;
}

interface AdoptionRequest {
  userId: number;
  userMessage: string;
}
