import { Animal } from "@/shared/type/animal";

interface AnimalSpecies {
  speciesId: number;
  species: string;
  breed: string;
  breedName: string;
  speciesName: string;
}
