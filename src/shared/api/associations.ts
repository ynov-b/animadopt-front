import { API_ENDPOINTS } from "@/shared/endpoint";
import { AssociationType } from "@/shared/type/associations";
import AssociationRequest from "../type/AssociationRequest";

export async function apiGetAssociations(): Promise<AssociationType[]> {
  try {
    const response = await fetch(API_ENDPOINTS.ASSOCIATIONS.ASSOCIATIONS, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des associations:", error);
    throw error;
  }
}

export async function apiGetAssociationId(
  assocId: number
): Promise<AssociationType> {
  try {
    const response = await fetch(
      API_ENDPOINTS.ASSOCIATIONS.ASSOCIATIONS_ID(assocId),
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des associations:", error);
    throw error;
  }
}

export async function apiGetAnimalsInAssociation(
  assoId: number
): Promise<Animal[]> {
  try {
    const response = await fetch(
      API_ENDPOINTS.ASSOCIATIONS.ANIMALS_IN(assoId),
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await response.json();
  } catch (error) {
    console.error(
      "Erreur lors de la récupération des animaux de l'association:",
      error
    );
    throw error;
  }
}

export async function apiGetRequestsInAssociation(
  assocId: number
): Promise<AssociationRequest[]> {
  try {
    const response = await fetch(
      API_ENDPOINTS.ASSOCIATIONS.REQUESTS_IN(assocId),
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des requetes:", error);
    throw error;
  }
}
