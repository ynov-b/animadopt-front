import {API_ENDPOINTS} from "@/shared/endpoint";
import {AssociationType} from "@/shared/type/associations";

interface Form {
  email: string;
  pwd: string;
}

export async function apiLogin(form: Form): Promise<AssociationType[]> {
  try {
    const response = await fetch(API_ENDPOINTS.ASSOCIATIONS.ASSOCIATIONS, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form)
    });

    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return await response.json();

  } catch (error) {
    console.error("Erreur lors de la connexion:", error);
    throw error;
  }
}
