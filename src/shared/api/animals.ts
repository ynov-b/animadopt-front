import { API_ENDPOINTS } from "@/shared/endpoint";

export async function apiGetAnimals(): Promise<Animal[]> {
  try {
    const response = await fetch(API_ENDPOINTS.ANIMALS.ANIMALS, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des animaux:", error);
    throw error;
  }
}

export async function apiGetAnimal(id: number): Promise<Animal> {
  try {
    const response = await fetch(API_ENDPOINTS.ANIMALS.ANIMALS_ID(id), {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération de l'animal:", error);
    throw error;
  }
}

export async function apiGetAnimalsInAssociation(
  assocId: number,
): Promise<Animal[]> {
  try {
    const response = await fetch(
      API_ENDPOINTS.ASSOCIATIONS.ANIMALS_IN(assocId),
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      },
    );

    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des animaux:", error);
    throw error;
  }
}

export async function createAnimal(assoId: number, animalData: Animal): Promise<Animal> {
  try {
    const response = await fetch(API_ENDPOINTS.ASSOCIATIONS.CREATE_ANIMAL(assoId), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        animalData,
      }),
    });

    if (!response.ok) {
      throw new Error(`Failed to create animal. Status: ${response.status}`);
    }

    return await response.json();
  } catch (error) {
    console.error("Error creating animal:", error);
    throw error;
  }
}

