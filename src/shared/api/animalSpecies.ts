import { API_ENDPOINTS } from "@/shared/endpoint";
import { AnimalSpecies } from "@/shared/type/animalSpecies";

export async function apiGetAllAnimalSpecies(): Promise<AnimalSpecies[]> {
  try {
    const response = await fetch(API_ENDPOINTS.ANIMAL_SPECIES.ANIMAL_SPECIES, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    return await response.json();
  } catch (error) {
    console.error("ERROR:", error);
    throw error;
  }
}

export async function apiGetAnimalSpecies(id: string): Promise<AnimalSpecies> {
  try {
    const response = await fetch(API_ENDPOINTS.ANIMAL_SPECIES.SPECIES_ID(id), {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    return await response.json();
  } catch (error) {
    console.error(
      "Erreur lors de la récupération de l'espèce de l'animal:",
      error,
    );
    throw error;
  }
}
