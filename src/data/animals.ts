export const animals: Animal[] = [
    {
        id: 1,
        name: "Fido",
        type: "Chien",
        age: 3,
        weight: 22,
        color: "Blanc",
        description: "Fido est un chien affectueux et énergique. Il aime jouer à la balle et se promener au parc.",
        image: "dog.jpg"
    },
    {
        id: 2,
        name: "Whiskers",
        type: "Serpent",
        age: 2,
        weight: 5,
        color: "Vert",
        description: "Whiskers est un serpent calme et curieux. Il est facile à entretenir et fascine les observateurs.",
        image: "snake.jpg"
    },
    {
        id: 3,
        name: "Buddy",
        type: "Chien",
        age: 5,
        weight: 18,
        color: "Blanc",
        description: "Buddy est un chien loyal et protecteur. Il est un excellent compagnon pour les promenades et la sécurité de la maison.",
        image: "dog.jpg"
    },
    {
        id: 4,
        name: "Fluffy",
        type: "Moufette",
        age: 1,
        weight: 4,
        color: "Noir et blanc",
        description: "Fluffy est une moufette douce et curieuse. Elle est apprivoisée et prête à vivre dans un environnement adapté.",
        image: "moufette.jpg"
    },
    {
        id: 5,
        name: "Mittens",
        type: "Lapin",
        age: 4,
        weight: 7,
        color: "Gris",
        description: "Mittens est un lapin amical et doux. Il adore les caresses et les légumes frais.",
        image: "rabbit.jpg"
    },
    {
        id: 6,
        name: "Rex",
        type: "Chien",
        age: 6,
        weight: 20,
        color: "Noir",
        description: "Rex est un chien robuste et intelligent. Il est bien dressé et prêt à devenir un membre de votre famille.",
        image: "dog.jpg"
    }
];
