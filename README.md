# Animadopt

## Description

Animadopt is an application designed to facilitate the adoption process for animals. It consists of two main components: the back-end (`animadopt-back`) and the front-end (`animadopt-front`).

## Launching the Application

### animadopt-back

1. Navigate to the `animadopt-back` directory.
2. Run the following commands:

   ```bash  
   mvn clean install  

3. Start the application.

### animadopt-front

1. Navigate to the `animadopt-front` directory.
2. Run the following commands:

   ```bash  
   npm i  
   npm run dev  


## Technologies Used

### Back-end
- Spring Boot
- Maven
- Hibernate
- PostgreSQL

### Front-end
- Next.js
- Tailwind CSS

## Deployment
The application is deployed on a Linux server running Ubuntu, hosted on Digital Ocean.

## Dependencies and Front-end Information
- **Next/Router:** Used for page routing.
- **React:** The core library for building the user interface.
- **Radix:** Utilized for form handling and styling.
- **Tailwind CSS:** A utility-first CSS framework for styling.
- **EsLint:** A linter for identifying and fixing problems in the JavaScript code.
- **TenStack (Query):** A tool for querying data in the application.

## Mock 
[Figma, Mock-up](https://www.figma.com/file/jaRrXaZmjilel0fgdXEWYM/Mock-up?type=design&node-id=0%3A1&mode=design&t=gTeHdzKgG8w245Ex-1)

## Additional Information
- Ensure that the necessary dependencies are installed before launching the application.
- The back-end utilizes Spring Boot, Maven, and Hibernate for efficient server-side operations.
- The front-end is developed using Next.js and Tailwind CSS for a responsive and visually appealing user interface.
- Data is stored in a PostgreSQL database to maintain a robust and scalable data management system.
